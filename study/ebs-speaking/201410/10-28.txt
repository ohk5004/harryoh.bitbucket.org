Marriage Compatibility  궁합



Extra Topics for study groups
1. Have you and your significant other got your gunghap checked by a fortune teller? Why or why not?
2. Do you believe in what fortune tellers say?
3. Why do you think couples get their gunghap checked?



Topic
Many Korean couples get their compatibility read by a fortune teller before marriage. Koreans call this gunghap. This usually happens when marriage is the next step in a relationship. In some ways, a couple's gunghap can be just as important as their parents' blessings. You first give the fortune teller your date and time of birth. This obviously goes for your partner, too. Then, the fortune teller tells you how harmonious your marriage will be. Younger generations don't place much weight on checking their gunghaps. But older generations take it seriously. There's really no need to put your marriage on the line over it though. Many couples just do it for fun.


많은 한국 커풀들은 결혼 전에 결혼하면 얼마나 잘 맞을지 점쟁이에게 궁합을 봅니다. 보통은 사귀다가 결혼을 앞둔 시점에서 궁합을 보게 됩니다. 어떤 면에서는 두 사람의 궁합이 부모님의 축복만큼이나 중요합니다. 궁합을 보려면 점쟁이에게 본인의 생년월일과 태어난 시간을 알려 주어야 합니다. 물론 상대방도 마찬가지입니다. 그러면 점쟁이는 앞으로 두 사람의 결혼이 얼마나 잘 맞을지 점을 칩니다. 요즘 젊은 세대들은 궁합에 큰 의미를 두지 않습니다. 그러나 어르신들은 궁합을 중요하게 생각합니다. 사실 궁합에 결혼의 모든 것을 걸 필요까지는 없습니다. 단순히 재미로 궁합을 보는 커플도 많습니다.





Expression‍‍‍‍ of the day
Do you believe in fortune telling?  점쟁이들이 하는 거 믿니?

A : Do you believe in fortune telling?
B : Yes and no.
A : What do you mean?
B : I think they do have a point, but I don't take it too seriously.

A : 점쟁이들이 하는 거 믿니?
B : 그렇기도 하고, 아니기도 하고.
A : 무슨 뜻이야?
B : 맞는 점도 있다고 생각하지만, 진지하게 받아들이지는 않아.