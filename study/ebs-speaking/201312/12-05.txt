﻿Cooking Food 음식 해 먹기


 
Extra Topics for study groups
1. How often do you cook instant noodles yourself? What is your favorite flavor? 
2. What do you like to put in your instant noodles?
   Green onions, eggs, pepper, rice cake or cheese? 
3. Do you prefer regular ramyun or cup-ramyun? Why is that? 
 

Topic
I'm not the best cook out there, but I know how to cook to some degree. Now, one of the most typical things I cook is instant noodles called ramyun. Now, what you basically do is: you boil two or three cups of water in a pot. And then, you put the soup base in. And when the water boils, you put in the noodles. You can add chopped-up green onions or an egg. You cook it for two or three more minutes until the noodles are fully cooked. Ramyun is one of the most typical snack foods Koreans would cook. It is cheap and there are various flavors.  


Expression‍ of the day
My mom is the best cook. 우리 어머니는 음식을 진짜 잘하셔.  
 
A : Do you like your mom's cooking?  
B : Oh, you best! My mom is the best cook. 
A : What is her specialty?  
B : She used to cook all sorts of Korean stews very well. 
 
A : 너는 어머니가 해 주시는 음식을 좋아하니? 
B : 당연하지! 우리 어머니는 음식을 진짜 잘하셔. 
A : 어떤 음식을 가장 잘하시는데?  
B : 온갖 종류의 한국 찌개를 정말 잘 끓이셨어. 