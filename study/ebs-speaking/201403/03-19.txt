Adolescence 사춘기

 

 
 
Extra Topics for study groups
1. What was your biggest conflict with your parents in your adolescence?  
2. How were you when you were a teenager? Do you think you gave your parents a hard time?
3. Talk about your friends in your teenage years. What were they like?  

 
 
 
Topic
I remember being a little rebellious in my adolescence. I kept secrets from my parents all the time. They didn't like me talking on the phone for a long time either. I had a curfew too. I got into trouble because I broke it from time to time. I would occasionally get into fights. My mother had to come to my school when that happened. However, the biggest problem I had with my parents was over my grades. I was trying my best, but I was simply not getting the grades I wanted. Overall, I did give my parents a hard time when I was a teenager. I feel sorry for how I behaved.
 
저는 사춘기 시절에 다소 반항적이었던 것으로 기억합니다. 저는 항상 부모님께 비밀을 갖고 있었습니다. 부모님께서는 제가 전화 통화를 오래 하는 것도 좋아하지 않으셨습니다. 그리고 통금도 정해져 있었지만 가끔은 통금 시간을 어겼기 때문에 부모님께 혼이 났습니다. 저는 친구들과 싸운적도 있습니다. 그런 일이 생기면 어머니께서 학교에 오셔야 했습니다. 그러나 부모님과의 가장 큰 갈등은 학교 성적에 관한 것이었습니다. 저는 최선을 다했지만 원하는 성적이 나오지 않았습니다. 전반적으로 보면, 저는 학창 시절에 부모님 속을 많이 썩였던 것 같습니다. 저의 행동에 대해 부모님께 죄송한 마음이 듭니다.
 

Expression‍ of the day
You son is very well-behaved. 아이들이 참 예의가 바르네요. 
A : Your son is very well-behaved. I like him so much. 
B : Well, thank you. 
A : I wish my son was like that too. 
B : How is your son? 

A : 아이들이 참 예의가 바르네. 정말 마음에 든다. 
B : 응, 고마워. 
A : 내 아들도 저랬으면 좋겠다. 
B : 네 아들은 어떤데? 
