Back Problems 허리 통증


 
Extra Topics for study groups
1. Have you ever had back problems? What did you do to treat the condition?
2. What are some causes of back conditions?
3. Do you have any people around you who have had back problems?

 
 
 
 
Topic
There are many people with back problems. These conditions can begin at a young age because of bad posture. Students typically slouch over their desks. This puts a lot of pressure on their backs. It could lead to conditions such as scoliosis. This is when the shape of the spine curves the wrong way. Disc-related conditions are very common among older people. The discs between the bones in your spine get worn out with age. This can cause severe back pain. The best way to avoid back conditions is to take preventative measures. This woud mean having a correct posture and getting regular exercise.
 

Expression‍ of the day
My mom got surgery on her back. 어머니께서 허리 수술을 받으셨어.  
A : Why are you going to the hospital? 
B : My mom got surgery on her back last month. 
A : Oh, really? Is she okay? 
B : Yeah, she's steadily recovering. 

A : 병원에는 왜 가니?  
B : 어머니께서 지난달에 허리 수술을 받으셨어. 
A : 아, 정말? 괜찮으시니? 
B : 응, 천천히 회복 중이셔. 
