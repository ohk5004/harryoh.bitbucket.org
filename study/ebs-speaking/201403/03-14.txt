Korean Pizzas 한국의 피자


 
Extra Topics for study groups
1. How often do you order pizza? What kind of pizza do you order mosf often?
2. What are some pizza toppings you like or dislike?
3. How are Korean pizzas and Western pizzas different in your point of view?

 
 
 
Topic
There are tons of pizza places all around the country in Korea. Korea has its own style when it comes to pizza toppings. Of course, Korean pizzas use most of the typical toppings, such as pepperoni, mushrooms, onions, and green peppers. But there are some toppings you can probably only find in Korea. Some examples are corn, potatoes, shrimp, barbequed meat or mashed sweet popato. And that's just the tip of the iceberg. Plus, Koreans don't really customize their pizzas that much. Instead, we just tend to order a pre-set pizza by its name. These days, people order pizza online and also with their smartphone apps.
 

Expression‍ of the day
There are 24-hour pizza places. 24시간 영업하는 피자 가게들이 있어. 

A : I'm hungry. 
B : Do you want to order a pizza?
A : At this hour? It's almost midnight.
B : There are 24-hour pizza places.

A : 나 배고파. 
B : 피자 주문할까? 
A : 이 시간에? 거의 자정이 다 되어 가는데. 
B : 24시간 영업하는 피자 가게들이 있어. 
