Rest Areas 휴게소


 
Extra Topics for study groups
1. What kind of snacks do you usually have at rest area?
    Why do you like having those snacks?
2. Talk about a rest area you liked or disliked.
3. Talk about a memorable thing that happened at a rest area.  

 
 
 
Topic
Highway rest areas or rest stops in Korea are not just pit stops. They are more like a one-stop full package service. The restrooms are huge in size, so you don't have to wait in line to use them. If you are hungry, you can head over to the food court. They sell everything from hot noodles to dumplings to Korean stews. There are also plenty of snacks you can get at rest areas as well. Potato balls, buttered squid, and walnut cookies are some of the popular snacks. And of course, they always have a convenience store where you can buy stuff and a gas station where you can fill up your gas tank.
 
한국의 고속도로 휴게소는 단순히 여행 중에 들르는 화장실이 아닙니다. 그보다는 한 번에 모든 서비스를 제공하는 시설에 가깝습니다. 고속도로 휴게소의 화장실은 매우 크기 때문에 줄을 설 필요도 없습니다. 배가 고프면 푸드코트에 가면 됩니다. 푸드코트에는 뜨거운 면류, 만두, 그리고 찌개에 이르기까지 없는 것이 없습니다. 뿐만 아니라 휴게소에서는 다양한 종류의 간식도 즐길 수 있습니다. 통감자 구이, 버터구이 오징어, 그리고 호두 과자와 같은 음식이 가장 인기가 있습니다. 그리고 모든 휴게소에는 항상 편의점이 있어 이것저것 살 수도 있고 주유소에서 기름을 넣을 수도 있습니다.
 

Expression‍ of the day
Can we pull over/up at a rest stop?  휴게소에 잠깐 들를 수 있어? 
A : Can we pull over at a rest stop? 
B : Really? 
A : Yeah, I need to use the bathroom. 
B : No problem. There's one 10 kilometers ahead. 

A : 휴게소에 잠깐 들를 수 있어?  
B : 정말? 
A : 응, 화장실에 좀 가야 해. 
B : 그러지 뭐. 10 킬로만 가면 하나 있어. 
