Demilitarized Zone(DMZ) 비무장 지대


 
Extra Topics for study groups
1. What comes to your mind when you think of the DMZ?
2. Talk about how Korea was divided into the North and South.
3. Do you remember watching any movies related to North and South Korea?
What was it about?

 
 
 
Topic
Korea is divided into North and South Korea. The two countries are still technically at war as there is no permanet peace treaty between them. The demilitarized zone(DMZ) is a strip of land between the two nations. It serves as a buffer zone between the twon Koreas. The area is off limits to humans. The area is very dangerous as it is still covered with landmines. With no human activity, it has become a haven for wildlife. In fact, there are plans to keep it a natural reserve after reunification. Foreign tourists come to the DMZ to get tours. Koreans are currently not allowed to visit the DMZ.
 
대한민국은 남한과 북한으로 나뉘어져 있습니다. 남북한 사이에 영구적인 평화 협정이 체결되지 않았기 때문에, 엄밀히 말해 양국은 여전히 전시 상태입니다. 비무장지대(DMZ)는 남북한 사이에 길게 뻗은 분계선으로, 양국 간의 완충 지대 역할을 합니다. 비무장 지대는 사람의 접근이 금지되어 있습니다. 이 지역은 아직도 지뢰로 뒤덮여 있어 매우 위험합니다. 그러나 사람들의 발길이 전혀 닿지 않으면서 이 지역은 야생 동물의 천국이 되었습니다. 사실 다시 통일이 되고 나서도 이 지여을 자연 보호 구역으로 보존하자는 계획도 있습니다. 해외 관광객들은 비무장 지대에 와서 관광을 합니다. 한국인들은 현재 비무장지대를 방문하는 것이 금지되어 있습니다.
 

Expression‍ of the day
The Korean War ended with an armistice treaty. 한국 전쟁은 휴전 협정으로 끝났어.

A : When did the Korean War break out?
B : It started in 1950 and ended in 1953.
A : How did it end?
B : The korean War ended with an armistice treaty.

A : 한국 전쟁은 언제 일어났니? 
B : 1950년에 발발해서 1953년에 종식되었어.
A : 어떻게 끝났는데?
B : 한국 전쟁은 휴전 협정으로 끝났어.
